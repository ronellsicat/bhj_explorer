# README #

### Set up ###

1. Download all source code.
2. Prepare your data. You need to put your images inside web/data/images folder and your patches.csv file inside web/data folder. The images folder will contain the image displayed for each patch where the filename is xxx.png where xxx is the patch_id entry in the patches.csv file (patch_id should be the row number of the patch). The patches.csv file contains the data for all patches. Take a look at the example web/data/example_patches.csv for guidance - entries should be self explanatory. Each row is a patch. The vector entry is the high-dimensional vector used for the patch as input to the T-SNE projection e.g. curvature histogram.
3. Open WebStorm. You can download it from here: https://www.jetbrains.com/webstorm/
4. In WebStrom, go to File->Open then select the "web" folder and click OK. Double click index.html from the file browswer on the left panel. Then click on the html and press Alt + F2. In the pop-out menu, select your preferred browswer (tested on Chrome). The webpage should open up and run.
5. Explore your data. You can change parameters in the following js files: main.js, params.js, performance.js. Whenever you change something in the js files, make sure to reload (F5 on Chrome) your browswer to reflect the changes.
6. To hide "download" info whenever a new patch is selected/highlighted, install these two Chrome plugins (obviously only works on Chrome browser):
	a. https://chrome.google.com/webstore/detail/downloads-your-download-b/gjihnjejboipjmadkpmknccijhibnpfe

	b. https://chrome.google.com/webstore/detail/downloads-overwrite-exist/fkomnceojfhfkgjgcijfahmgeljomcfk


### Notes ###

1. When you add variables or dimensions to your patch data such as performance metrics or parameters, make sure to update the "variables_to_hide" variables in params.js and performance.js to appropriately hide the variables you don't want to show up in the parallel coordinates.
2. If you are using volume_id as variable_for_coloring in performance.js or params.js, make sure that coloring_is_linear is set to 0 and make sure that coloring_domain covers the min and max possible values of volume_id.
3. If you are using a different variable for variable_for_coloring, make sure you choose properly between linear scaling and ordinal scaling and make sure to set the possible data range in coloring_domain.
4. Screenshot of what to expect can be seen in the [wiki](https://bitbucket.org/ronellsicat/bhj_explorer/wiki/).


### Features ###

1. Dynamically set T-SNE parameters: perplexity, epsilon.
2. Specify query (filter) via parallel coordinates. The user can brush on any dimension of the parallel coordinates to highlight patches of interest. Then the user can press "Apply Query" to only display data within the query. Press "Reset Data" to get back to original data.
3. When user clicks on a patch image, its lines in the parallel coordinates plots are highlighted. User can clear highlights by clicking "Clear Highlights" button under the parallel coords plots. The info of the highlighted patch is also saved to a text file called bhj_selected_patch.txt which is automatically saved in the Downloads directory of Chrome. This is only tested to work on Chrome browser.
4. When user clicks on "Toggle Query" button the t-sne plot is updated to show filtered/queried patches or not.
5. User can zoom in and out and also pan around the t-sne plot to explore the projection.
6. User can show histogram of patches for a user specified data variable - user can type the variable name in the provided input form. User can also adjust the number of bins of the histogram. When the user performs queries, the histogram is automatically updated.
7. Patches of medoids now have text labels with color orange to easily identify them in the tsne view.
8. User can toggle between showing only medoids and showing all patches by pressing "Medoids Only" button.
9. When user presses CTRL + click on any patch, only the patches belonging to the same cluster as the patch will be displayed and the rest will be invisible. To view all patches again, CTRL + click on any patch.
10. Play/pause the tsne algorithm via the "Toggle" button.
11. Medoids table view is shown in the bottom of the page.
12. Bigger view of patch is shown when hovering on top of a patch in the tsne view.


### Features TODO ###
1. Visualize clustering from other method using cluster_id info. For now, as a workaround, you can preprocess the images and add a color coded border/margin to the images where the color corresponds to the cluster_id of the patch. Maybe this can be coded in Matlab where the images are also computed.


### References ###

T-SNE implementation is from:
https://github.com/karpathy/tsnejs

T-SNE tutorial on params:
http://distill.pub/2016/misread-tsne/

Parallel coordinates is from:
https://syntagmatic.github.io/parallel-coordinates/
