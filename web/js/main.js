// PARAMS:
var csv_filename = "data/patches.csv";


// PARAMS for T-SNE:
tsne_epsilon = 10;
tsne_perplexity = 3;
d3.select("#learning-rate-input").attr("value", tsne_epsilon);
d3.select("#perplexity-input").attr("value", tsne_perplexity);

console.log(tsne_epsilon);
console.log(tsne_perplexity);

// PARAMS for T-SNE DISPLAY:
var patch_display_size = 32;
var vis = [];
vis.margin = { top: 10, right: 10, bottom: 10, left: 10 };
vis.max_height = 800;
var show_labels = true;	// display labels of patches (0 is false, 1 is true)
var show_tooltips = true;

// PARAMS for HISTOGRAM:
var histogram_num_bins = 10;
var histogram_data_name = 'BN';
d3.select("#histogram-var").attr("value", histogram_data_name);
d3.select("#histogram-numbins").attr("value", histogram_num_bins);

// PARAMS for MEDOIDS TABLE:
var medoids_display_size = 128;
var space_x = 10;
var space_y = 5;
var label_size = 12;






// Global variables:
var patch_data_orig;		// Contains original data
var patch_data_queried;		// Contains data that is currently displayed (after query)
var parcoords_perf;			// Holds parcoords: interact with this variable from a javascript console
var parcoords_params;
var tsne_patch_data = [];
tsne_patch_data.mat = [];
tsne_patch_data.ids = [];	// used for loading images
tsne_patch_data.labels = [];	// used for displaying beside images
var showQueryInTsne = true;
var histogram_data = [];
var tsneUpdate;
var intervalMS = 0;
var keepUpdatingTSNE = true;
var cluster_medoids_patch_id = [];
var cluster_medoids = [];
var query_opacity_per_patch;
var medoid_opacity_per_patch;
var cluster_opacity_per_patch;
var medoids_pos = [];

// Main entry point (runs on page load):
$(window).load(function() {
		
	// Load patch data:
	d3.csv(csv_filename, function(data){

		patch_data_orig = data;
		patch_data_queried = data;
		
		ResetOpacities();
		
		// Convert each vector into a row array:
		for(i = 0; i < data.length; i++) {

			row = data[i].vector.split(',').map(Number);
			tsne_patch_data.mat.push(row);
			//console.log(row);

			// Save cluster_id as a label:
			tsne_patch_data.labels.push(data[i].cluster_id);
			tsne_patch_data.ids.push(data[i].patch_id);
			
			if(data[i].medoid == 1) {
				cluster_medoids_patch_id.push(i);
				cluster_medoids.push(-1);
				
				console.log('medoid bool ' + data[i].medoid + " for patch " + i);				
			}
		}
		
		for(m = 0; m < cluster_medoids_patch_id.length; m++) {
			
			cluster_medoids[ data[cluster_medoids_patch_id[m]].cluster_id - 1 ] = cluster_medoids_patch_id[m];
			medoids_pos.push([0,0]);
			
			console.log('cluster id ' + data[cluster_medoids_patch_id[m]].cluster_id + ' patch id ' + cluster_medoids_patch_id[m]);
		}
		
		for(mm = 0; mm < cluster_medoids.length; mm++) {
			console.log('medoid ' + mm + " = " + cluster_medoids[mm]);
		}

//		console.log(tsne_patch_data);

		T.initDataRaw(tsne_patch_data.mat); // init embedding
		drawEmbedding(); // draw initial embedding
		
		drawMedoidsTable();

		//T.debugGrad();
		tsneUpdate = setInterval(step, intervalMS);
		//step();


		// Create Performance ParCoords:
		UpdatePerformanceParCoords(patch_data_queried);

		// Create Parameters ParCoords:
		UpdateParamsParCoords(patch_data_queried);

		RenderHistogram(patch_data_queried, histogram_data_name, histogram_num_bins);
	});
});




// Create T-SNE plot:

// Start of set up:
var opt = {epsilon: tsne_epsilon, perplexity: tsne_perplexity};
var T = new tsnejs.tSNE(opt); // create a tSNE instance
var Y;
var svg;
var divTooltip;
var showMedoidsOnly = false;
var showClusterOnly = false;

vis.width = $("#embed").width() - vis.margin.left - vis.margin.right;
vis.height = vis.max_height - vis.margin.top - vis.margin.bottom;


function updateEmbedding() {
	
		var Y = T.getSolution();
		svg.selectAll('.u')
			.data(tsne_patch_data.ids)
			.attr("opacity", function(d){
				return query_opacity_per_patch[d] * medoid_opacity_per_patch[d] * cluster_opacity_per_patch[d];
			})
			.attr("pointer-events", function(d){
						
						if(query_opacity_per_patch[d] * medoid_opacity_per_patch[d] * cluster_opacity_per_patch[d] == 1) {
							return "all";
						} else {
							return "none";
						}
					})
			.attr("transform", function(d, i) { 
				
				posX = ((Y[i][0]*20*ss + tx) + (vis.width/2));
				posY = ((Y[i][1]*20*ss + ty) + (vis.height/2));
				/*
				if(patch_data_orig[d].medoid == 1) {
					medoids_pos[patch_data_orig[d].cluster_id] = [posX, posY];
				}
				*/
				return "translate(" + posX
				 + "," + posY
				 + ")"; });

//	console.log('updated embedding');
}

var cur_selected_cluster_id = 0;

var translate_svg_to_cluster_medoid = true;
function UpdateClusterOpacity() {
	
	if(showClusterOnly) {
			
		for ( i = 0; i < patch_data_orig.length; i++) {

			if (patch_data_orig[i].cluster_id == cur_selected_cluster_id) {
				cluster_opacity_per_patch[i] = 1;
				console.log('found' + cur_selected_cluster_id);
				//							console.log('op ' + query_opacity_per_patch[d] * medoid_opacity_per_patch[d] * cluster_opacity_per_patch[d]);
				
				// Set the translation
				/*
				if(patch_data_orig[i].medoid == 1) {
					
					if(translate_svg_to_cluster_medoid) {
						
						var Y = T.getSolution();
						posX = ((Y[i][0]*20*ss + tx) + (vis.width/2));
						posY = ((Y[i][1]*20*ss + ty) + (vis.height/2));
						
						console.log("w " + vis.width + " h " + vis.height);
						tx = 0;
						ty = 0;			
					}
				}
				*/
			} else {
				cluster_opacity_per_patch[i] = 0;
			}
		}

	} else {
		cluster_opacity_per_patch = new Array(patch_data_orig.length).fill(1);
	}
}

var zoomListener;


function drawEmbedding() {
	$("#embed").empty();
	var div = d3.select("#embed");

	divTooltip = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 1).attr("pointer-events", "none");

	// get min and max in each column of Y
	var Y = T.Y;

	svg = div.append("svg")// svg is global
	.attr("width", vis.width).attr("height", vis.height);

	var g = svg.selectAll(".b").data(tsne_patch_data.ids).enter().append("g").attr("class", "u");

	// each data element is referenced by g -
	g.append("svg:image").attr('x', 0).attr('y', 2).attr('width', patch_display_size).attr('height', patch_display_size).attr("xlink:href", function(d) {
		return "data/images/" + d + ".png";
	}).on('click', function(d) {

		if (d3.event.ctrlKey) {

			cur_selected_cluster_id = patch_data_orig[d].cluster_id;
			console.log('expanding/contracting cluster ' + cur_selected_cluster_id);

			showClusterOnly = !showClusterOnly;

			//				if(showMedoidsOnly && showClusterOnly) {
			//					ToggleMedoidsVisibility();
			//				}

			UpdateClusterOpacity();

			updateEmbedding();

		} else {
			HighlightPatch(d);
		}

	}).on('mouseover', function(d) {
		//console.log('hovered' + d);

		if (show_tooltips) {

			img_code = "<span><h2>patch: " + d + "  cluster: " + tsne_patch_data.labels[d] + "</h2><img src=\"data/images/" + d + ".png\"></span>";

			divTooltip.html(img_code).style("left", (d3.event.pageX + 40) + "px").style("top", (d3.event.pageY + 40) + "px").style("opacity", 1).attr("pointer-events", "none");

		}

	}).on('mouseout', function(d) {
		//	console.log('out' + d);

		divTooltip.style("opacity", 1e-6).style("left", 1000000 + "px").style("top", 100000 + "px");
	});

	if (show_labels) {
		g.append("text").attr("text-anchor", "top").attr("font-size", 12).attr("fill", function(d) {
			if (patch_data_orig[d].medoid == 1) {
				return "orange";
			} else {
				return "black";
			}
		}).attr("font-weight", function(d) {
			if (patch_data_orig[d].medoid == 1) {
				return "bold";
			} else {
				return "normal";
			}
		}).attr("pointer-events", "none").text(function(d) {
			return tsne_patch_data.labels[d];
		});
	}

	zoomListener = d3.behavior.zoom().scaleExtent([0.1, 100]).center([0, 0]).on("zoom", zoomHandler);
	zoomListener(svg);
}

var num_medoids_per_row = 0;

function computeMedoidPosX(index) {

	return ( (index % num_medoids_per_row) * (medoids_display_size + space_x));
}

var Mheight = 0;

function computeMedoidPosY(index) {
	
	var row = Math.floor( index / num_medoids_per_row);
	console.log("pos y " + index + ", " + index * (medoids_display_size + space_y));
	return row * (medoids_display_size + space_y);
}

function drawMedoidsTable() {
	$("#medoids-table").empty();
	var divM = d3.select("#medoids-table");

	var Mwidth = $("#medoids-table").width(); // - vis.margin.left - vis.margin.right;
	Mheight = $("#medoids-table").height() - vis.margin.top - vis.margin.bottom;
	
	num_medoids_per_row = Math.floor( Mwidth / (medoids_display_size + space_x));
	
	var svgM = divM.append("svg")// svg is global
	.attr("width", Mwidth).attr("height", Mheight);

	var gM = svgM.selectAll(".b").data(cluster_medoids).enter().append("g").attr("class", "u");
	
	// each data element is referenced by g -
	gM.append("svg:image").attr('width', medoids_display_size).attr('height', medoids_display_size).attr("xlink:href", function(d) {
		
		console.log("loading image " + "data/images/" + d + ".png");
		return "data/images/" + d + ".png";
	})
	.attr("x", function(d,i) {
		return computeMedoidPosX(i);})
	.attr("y", function(d,i) {
		return computeMedoidPosY(i);
		});

		gM.append("text").attr("text-anchor", "top").attr("font-size", label_size).attr("fill", "black")
		.attr("y", function(d,i){
			
			return medoids_display_size + computeMedoidPosY(i) + (space_y/2);
		}).attr("x", function(d,i){
			return computeMedoidPosX(i) + (medoids_display_size/2);
		})
		.attr("font-weight", "normal").attr("pointer-events", "none").text(function(d,i) {
			return i+1;
		});
	
}


function UpdateMedoidsOpacity() {

	if (showMedoidsOnly) {

		for ( i = 0; i < patch_data_orig.length; i++) {

			if (patch_data_orig[i].medoid == 1) {
				medoid_opacity_per_patch[i] = 1;
			} else {
				medoid_opacity_per_patch[i] = 0;
			}
		}

	} else {
		medoid_opacity_per_patch = new Array(patch_data_orig.length).fill(1);
	}
}

function ToggleMedoidsVisibility() {

	showMedoidsOnly = !showMedoidsOnly;

	console.log('patchdata len is ' + patch_data_orig.length);
	console.log('patch data[0] ' + patch_data_orig[0].param_2);
/*
	var gg = svg.selectAll("g").style('opacity', function(d) {

		pp = patch_data_orig[d];
		if (pp.medoid == 1) {
			query_opacity_per_patch[d] = 1;
			return 1;
		} else {
			return 0;
		}
	}).attr("pointer-events", function(d) {

		if (patch_data_orig[d].medoid == 1) {
			return "all";
		} else {
			return "none";
		}

	});
*/
	
	UpdateMedoidsOpacity();

	updateEmbedding();
}


var tx=0, ty=0;
var ss=1;
function zoomHandler() {
	tx = d3.event.translate[0];
	ty = d3.event.translate[1];
	ss = d3.event.scale;
}

function step() {
	
	if(keepUpdatingTSNE) {
		for(var k=0;k<1;k++) {
			T.step(); // do a few steps
		}	
	}
	
	updateEmbedding();
}

function ResetOpacities() {
	
	query_opacity_per_patch = new Array(patch_data_orig.length).fill(1);
	medoid_opacity_per_patch = new Array(patch_data_orig.length).fill(1);
	cluster_opacity_per_patch = new Array(patch_data_orig.length).fill(1);
}

function HighlightPatch(patch_index) {

	console.log("User clicked on patch id: " + patch_index);

	ClearHighlights();

	parcoords_perf.highlight([patch_data_orig[patch_index]]);
	parcoords_params.highlight([patch_data_orig[patch_index]]);

	// Write the info of the patch to text file
	//patch_id	volume_id	patchx	patchy	patchz	psizex	psizey	psizez	cluster_id

	selected_patch = patch_data_orig[patch_index];
	WriteStringToDownloadedFile(
		selected_patch.patch_id +'\t'+
		selected_patch.volume_id +'\t'+
		selected_patch.patchx +'\t'+
		selected_patch.patchy +'\t'+
		selected_patch.patchz +'\t'+
		selected_patch.psizex +'\t'+
		selected_patch.psizey +'\t'+
		selected_patch.psizez +'\t'+
		selected_patch.cluster_id +'\n',
		'bhj_selected_patch.txt', 'text/plain');
}


function ClearHighlights() {

	parcoords_perf.unhighlight();
	parcoords_params.unhighlight();
}

function ToggleTSNE() {

	//clearInterval(tsneUpdate);	
	keepUpdatingTSNE = !keepUpdatingTSNE;

}

function  UpdateQueryOpacity() {
	
	// Create an opacity vector for each item present in data and fill it with 0s and 1s:
	var num_patches_orig = patch_data_orig.length;
	if(showQueryInTsne) {

		query_opacity_per_patch = new Array(num_patches_orig).fill(0);
		patch_data_queried.forEach(function(d){
			query_opacity_per_patch[d.patch_id] = 1;
		});
	} else {

		query_opacity_per_patch = new Array(num_patches_orig).fill(1);
	}
}

function ToggleQueryVisibility() {

	showQueryInTsne = !showQueryInTsne;

	UpdateQueryOpacity();
	
	updateEmbedding();
}


function UpdateParCoordsData(data) {

	if(data == false) {
		return;
	}

	patch_data_queried = data;

	UpdatePerformanceParCoords(patch_data_queried);
	UpdateParamsParCoords(patch_data_queried);

	UpdateMetricHistogram();
}


function ApplyQueryOnPerf() {

	selected_data = parcoords_perf.brushed();
	UpdateParCoordsData(selected_data);
	
	UpdateQueryOpacity();
	updateEmbedding();
}

function ApplyQueryOnParams() {

	selected_data = parcoords_params.brushed();
	UpdateParCoordsData(selected_data);
	
	UpdateQueryOpacity();
	updateEmbedding();
}


function ResetData() {

	patch_data_queried = patch_data_orig;
	UpdateParCoordsData(patch_data_queried);
	
	UpdateQueryOpacity();
	updateEmbedding();
}


function UpdateTSNE() {

	keepUpdatingTSNE = true;
	
	tsne_epsilon = document.getElementById("learning-rate-input").value;
	tsne_perplexity = document.getElementById("perplexity-input").value;

	console.log(tsne_perplexity);
	console.log(tsne_epsilon);

	// Clean up element before creating new coords:
	var myNode = document.getElementById("embed");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}

	T.initDataRaw(tsne_patch_data.mat); // init embedding
	drawEmbedding(); // draw initial embedding

	//T.debugGrad();
	tsneUpdate = setInterval(step, intervalMS);
	//step();
}


function GetFields(input, field) {
	return input.map(function(o) {
		return o[field];
	});
}

function UpdateMetricHistogram() {

	var data_name_for_hist = document.getElementById("histogram-var").value;
	console.log(data_name_for_hist);

	var num_bins = document.getElementById("histogram-numbins").value;

	RenderHistogram(patch_data_queried, data_name_for_hist, num_bins);
}





function WriteStringToDownloadedFile(strData, strFileName, strMimeType) {
	var D = document,
		A = arguments,
		a = D.createElement("a"),
		d = A[0],
		n = A[1],
		t = A[2] || "text/plain";

	//build download link:
	a.href = "data:" + strMimeType + "charset=utf-8," + escape(strData);


	if (window.MSBlobBuilder) { // IE10
		var bb = new MSBlobBuilder();
		bb.append(strData);
		return navigator.msSaveBlob(bb, strFileName);
	} /* end if(window.MSBlobBuilder) */



	if ('download' in a) { //FF20, CH19
		a.setAttribute("download", n);
		a.innerHTML = "downloading...";
		D.body.appendChild(a);
		setTimeout(function() {
			var e = D.createEvent("MouseEvents");
			e.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			a.dispatchEvent(e);
			D.body.removeChild(a);
		}, 66);
		return true;
	}; /* end if('download' in a) */



	//do iframe dataURL download: (older W3)
	var f = D.createElement("iframe");
	D.body.appendChild(f);
	f.src = "data:" + (A[2] ? A[2] : "application/octet-stream") + (window.btoa ? ";base64" : "") + "," + (window.btoa ? window.btoa : escape)(strData);
	setTimeout(function() {
		D.body.removeChild(f);
	}, 333);
	return true;
}
