// PARAMS:
var variables_to_hide =  ["psizex", "psizey", "x", "y", "z","MC", "d", "k1", "k2", "vector"];
var variable_for_coloring = "volume_id";
var coloring_domain = [0, 120]; 			// min-max values for coloring
var coloring_range = colorbrewer.RdBu[9];	// range of colors used for coloring
var coloring_is_linear = 0;					// set this to 1 for linear color scaling; 0 for ordinal
var coloring_opacity = 0.6;


var get_color = d3.scale.ordinal()
	.range(["#a6cee3","#1f78b4","#b2df8a","#33a02c",
		"#fb9a99","#e31a1c","#fdbf6f","#ff7f00",
		"#cab2d6","#6a3d9a","#ffff99","#b15928"]);


// linear color scale
var get_color_old = d3.scale.ordinal()
	.domain(coloring_domain)
	.range(coloring_range);

if(coloring_is_linear == 1) {
	get_color = d3.scale.linear()
		.domain(coloring_domain)
		.range(coloring_range);
}

function UpdatePerformanceParCoords(data) {

	// Clean up element before creating new coords:
	var myNode = document.getElementById("performance");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}

	parcoords_perf = d3.parcoords()("#performance")
		.data(data)
		.mode("queue")
		//.rate(100)
		.hideAxis(variables_to_hide)
		.composite("darken")
		.color(function (d) { return get_color(d[variable_for_coloring]); })  // quantitative color scale
		.alpha(coloring_opacity)
		.render()
		.brushMode("1D-axes")  // enable brushing
		.interactive()  // command line mode

	parcoords_perf.brushReset();

	var explore_count = 0;
	var exploring = {};
	var explore_start = false;
	parcoords_perf.svg
		.selectAll(".dimension")
		.style("cursor", "pointer")
		.on("click", function (d) {
			exploring[d] = d in exploring ? false : true;
			event.preventDefault();
			if (exploring[d]) d3.timer(explore(d, explore_count));
		});

	function explore(dimension, count) {
		if (!explore_start) {
			explore_start = true;
			d3.timer(parcoords_perf.brush);
		}
		var speed = (Math.round(Math.random()) ? 1 : -1) * (Math.random() + 0.5);
		return function (t) {
			if (!exploring[dimension]) return true;
			var domain = parcoords_perf.yscale[dimension].domain();
			var width = (domain[1] - domain[0]) / 4;

			var center = width * 1.5 * (1 + Math.sin(speed * t / 1200)) + domain[0];

			parcoords_perf.yscale[dimension].brush.extent([
				d3.max([center - width * 0.01, domain[0] - width / 400]),
				d3.min([center + width * 1.01, domain[1] + width / 100])
			])(parcoords_perf.g()
				.filter(function (d) {
					return d == dimension;
				})
			);
		};
	};
};