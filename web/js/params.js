// PARAMS:
var variables_to_hide2 =  [ "psizex", "psizey", "Dist", "Comp", "Orient","Diff", "Abs", "Tort", "BN", "vector"];
var variable_for_coloring2 = "volume_id";
var coloring_domain2 = [0, 120]; 			// min-max values for coloring
var coloring_range2 = colorbrewer.RdBu[9];	// range of colors used for coloring
var coloring_is_linear2 = 0;					// set this to 1 for linear color scaling; 0 for ordinal
var coloring_opacity2 = 1.0;


var get_color2 = d3.scale.ordinal()
	.range(["#a6cee3","#1f78b4","#b2df8a","#33a02c",
		"#fb9a99","#e31a1c","#fdbf6f","#ff7f00",
		"#cab2d6","#6a3d9a","#ffff99","#b15928"]);

// linear color scale
	var get_color2_old = d3.scale.ordinal()
		.domain(coloring_domain2)
		.range(coloring_range2);

	if(coloring_is_linear2 == 1) {
		get_color2 = d3.scale.linear()
			.domain(coloring_domain2)
		.range(coloring_range2);
}

function UpdateParamsParCoords(data) {

	// Clean up element before creating new coords:
	var myNode = document.getElementById("params");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}

	parcoords_params = d3.parcoords()("#params")
		.data(data)
		.mode("queue")
		//.rate(100)
		.hideAxis(variables_to_hide2)
		.composite("darken")
		.color(function (d) { return get_color2(d[variable_for_coloring2]); })  // quantitative color scale
		.alpha(coloring_opacity2)
		.render()
		.brushMode("1D-axes")  // enable brushing
		.interactive()  // command line mode

	parcoords_params.brushReset();

	var explore_count = 0;
	var exploring = {};
	var explore_start = false;
	parcoords_params.svg
		.selectAll(".dimension")
		.style("cursor", "pointer")
		.on("click", function (d) {
			exploring[d] = d in exploring ? false : true;
			event.preventDefault();
			if (exploring[d]) d3.timer(explore(d, explore_count));
		});

	function explore(dimension, count) {
		if (!explore_start) {
			explore_start = true;
			d3.timer(parcoords_params.brush);
		}
		var speed = (Math.round(Math.random()) ? 1 : -1) * (Math.random() + 0.5);
		return function (t) {
			if (!exploring[dimension]) return true;
			var domain = parcoords_params.yscale[dimension].domain();
			var width = (domain[1] - domain[0]) / 4;

			var center = width * 1.5 * (1 + Math.sin(speed * t / 1200)) + domain[0];

			parcoords_params.yscale[dimension].brush.extent([
				d3.max([center - width * 0.01, domain[0] - width / 400]),
				d3.min([center + width * 1.01, domain[1] + width / 100])
			])(parcoords_params.g()
				.filter(function (d) {
					return d == dimension;
				})
			);
		};
	};
}
