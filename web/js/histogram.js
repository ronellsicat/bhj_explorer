// Chart area
//
var margin = {top: 60, right: 20, bottom: 40, left: 80},
	width =  $('#metric-histogram').width() - margin.left - margin.right,
	height = $('#metric-histogram').height() - margin.top - margin.bottom;

//width = width > 600 ? 600 : width;
//width = width < 400 ? 400 : width;

var svg2 = d3.select("#metric-histogram").append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");




// Scales and axes

var x = d3.scale.ordinal()
	.rangeRoundBands([0, width], .1);

var y = d3.scale.linear()
	.range([height, 0]);

var xAxis = d3.svg.axis()
	.scale(x)
	.orient("bottom");

var yAxis = d3.svg.axis()
	.scale(y)
	.orient("left").tickFormat(d3.format("d"));

var xAxisGroup = svg2.append("g")
	.attr("class", "x-axis axis");

var yAxisGroup = svg2.append("g")
	.attr("class", "y-axis axis");



function RenderHistogram(data, data_name, num_bins) {

	data = GetFields(data, data_name);
	console.log(data);

	histogram_data = ComputeHistogram(data, num_bins);

	// Update scale domains
	x.domain(histogram_data.map(function(d) { return d.key; }));
	y.domain([0, d3.max(histogram_data, function(d) { return d.values; })]);


	// ---- DRAW BARS ----

	var bars = svg2.selectAll(".bar")
		.data(histogram_data);

	// Add
	bars.enter().append("rect")
		.attr("class", "bar");

	// Update
	bars
		.attr("x", function(d) { return x(d.key); })
		.attr("width", x.rangeBand())
		.attr("y", function(d) { return y(d.values); })
		.attr("height", function(d) { return height - y(d.values); });


	// Remove
	bars.exit().remove();



	// ---- DRAW AXIS ----

	xAxisGroup = svg2.select(".x-axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis);

	yAxisGroup = svg2.select(".y-axis")
		.call(yAxis);

	svg2.select("text.axis-title").remove();

	svg2.append("text")
		.attr("class", "axis-title")
		.attr("x", -5)
		.attr("y", -15)
		.attr("dy", ".1em")
		.style("text-anchor", "end")
		.text("# Patches");
}


function ComputeHistogram(data, num_bins) {

	data.forEach(function(d,ii){
		data[ii] = +data[ii];
	});

	console.log(data);

	minval = d3.min(data);
	maxval = d3.max(data);

	console.log("hist min " + minval + ", hist max " + maxval);

	var intervals = [];
	var hist_data = [];
	var curmin = minval;
	var bin_width = (maxval - minval) / (num_bins-1);


	for(ii = 0; ii < num_bins; ii++) {

		var curmax = curmin+bin_width;
		intervals.push({minvv:curmin, maxvv:curmax});
		hist_data.push({key:"["+curmin.toFixed(2).toString()+" - "+curmax.toFixed(2).toString()+")", values:0});

		curmin = curmax;
	}

	for(dd = 0; dd < data.length; dd++) {

		for(ii = 0; ii < num_bins; ii++) {

			if (data[dd] >= intervals[ii].minvv && data[dd] <= intervals[ii].maxvv) {

				hist_data[ii].values = hist_data[ii].values + 1;
				continue;
			}
		}
	}

	console.log(intervals);
	console.log(hist_data);

	return hist_data;
}